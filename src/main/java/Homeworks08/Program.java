package Homeworks08;

public class Program {
    public static void main (String[] args) {
        Person[] persArray = new Person[10];

        persArray[0] = new Person("Анна", 69.2);
        persArray[1] = new Person("Петр", 73.0);
        persArray[2] = new Person("Наталья", 68.8);
        persArray[3] = new Person("Елизовета", 65.3);
        persArray[4] = new Person("Татьяна", 78.9);
        persArray[5] = new Person("Василий", 81.9);
        persArray[6] = new Person("Игорь", 76.0);
        persArray[7] = new Person("Елена", 67.5);
        persArray[8] = new Person("Иван", 72.8);
        persArray[9] = new Person("Алексей", 95.5);


        for (int i = 0; i < persArray.length - 1; i++) {
            double min = persArray[i].weight;
            int minIndex = i;
            for (int j = i; j < persArray.length; j++) {
                if (persArray[j].weight < min) {
                    min = persArray[j].weight;
                    minIndex = j;
                }
            }
            double temp = persArray[i].weight;
            persArray[i].weight = min;
            persArray[minIndex].weight = temp;
        }
        for (int i = 0; i < persArray.length - 1; i++){
            System.out.println("Имя: " + persArray[i].name + ", вес: " + persArray[i].weight + ";");
        }
        System.out.println("Имя: " + persArray[persArray.length - 1].name + ", вес: " + persArray[persArray.length - 1].weight + ".");

    }
}
