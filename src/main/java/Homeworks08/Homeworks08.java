public class Homeworks08 {

    public static void main(String[] args) {
        Human[] humans = new Human[10];
        humans[0] = new Human(82.9, "Петя");
        humans[1] = new Human(63.7, "Костя");
        humans[2] = new Human(56.7, "Жора");
        humans[3] = new Human(53.0, "Вася");
        humans[4] = new Human(90.9, "Илья");
        humans[5] = new Human(45.3, "Наташа");
        humans[6] = new Human(53.7, "Катя");
        humans[7] = new Human(51.7, "Лена");
        humans[8] = new Human(98.7, "Игорь");
        humans[9] = new Human(76.5, "Леша");

        for (int i = 0; i < humans.length - 1; i++) {
            double min = humans[i].weight;
            int minIndex = i;
            for (int j = i; j < humans.length; j++) {
                if (humans[j].weight < min) {
                    min = humans[j].weight;
                    minIndex = j;
                }
            }
            Human temp = humans[i];
            humans[i] = humans[minIndex];
            humans[minIndex] = temp;
        }
        for (int i = 0; i < humans.length - 1; i++) {
            System.out.println("Имя " + humans[i].name + ", вес " + humans[i].weight + ";");
        }
        System.out.println("Имя " + humans[humans.length - 1].name + ", вес " + humans[humans.length - 1].weight + ".");
    }
}

